# **アプリの画面**

"初期画面→機能1を使用後の画面→機能2を使用後の画面"の順番



![image](/uploads/4bbbf3e7cd8f41bc117b5d3b484ad973/image.png)
![image](/uploads/b07be91ff2fbbe6259331edcd764b318/image.png)
![image](/uploads/695b11f177e14a2727e4d31a4c742a60/image.png)

# **コンセプト図**

![image](/uploads/49c2d0b5c02691a15c68d964def5e403/image.png)

# **実装**

![image](/uploads/2eccfd88ca082258ba012d7903a31eea/image.png)

・**歩行数は1週間分の平均を求めて、それによって通知を送る日とコメントを設定する**

・**位置情報はアプリを開いた時に取得する**

・**本アプリでは、Google OAtuh 2.0認証とGoogle fitness API、OpenWeatherMap APIを使用しているため、それぞれのサイトで設定を行う必要がある**
