package com.example.osanpomanager;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

//import com.example.android.google.oauth.sample.logger.Log;
//import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
//import com.google.android.gms.games.event.Event;
/*import com.google.android.gms.plus.Account;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttendee;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.EventReminder;*/

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class AsyncHttpRequest /*extends AsyncTask<URL, Void, String>*/ {
    private int TODAY_FORCAST_INDEX = 0;
    private Activity mainActivity;
    private static final String TAG = "MainActivity";

    private AlarmManager am;
    private AlarmManager am2;
    private AlarmManager am3;
    private PendingIntent pending;
    private PendingIntent pending2;
    private PendingIntent pending3;
    private String result = "";

    public AsyncHttpRequest(Activity activity) {
        // 呼び出し元のアクティビティ
        this.mainActivity = activity;
    }

    /**
     * 非同期処理で天気情報を取得する.
     * @param //urls 接続先のURL
     * @return 取得した天気情報
     */

    private class AsyncRunnable implements Runnable {
        private URL urls;
        private int t;
        //private GoogleSignInAccount account;
        public AsyncRunnable(URL urls, int t) {
            this.urls = urls;
            this.t = t;
        }
        Handler handler = new Handler(Looper.getMainLooper());
        @Override
        public void run() {
            // ここにバックグラウンド処理を書く
            //Account act = (Account) this.account.getAccount();
            //Log.d(TAG, "デバッグ:" + act);
            //String address = this.account.getEmail();
            List<String> resultText;

            long days[] = new long[9];
            resultText = new ArrayList<>();
            HttpURLConnection connection = null;

            try {
                URL url = urls;
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.connect();



                int responseCode = connection.getResponseCode();

                if (responseCode == HttpURLConnection.HTTP_OK) {
                    InputStreamReader isr = new InputStreamReader(connection.getInputStream());
                    BufferedReader br = new BufferedReader(isr);
                    String str = br.readLine();
                    JSONObject json = new JSONObject(str);
                    JSONArray hourly = json.getJSONArray("hourly");
                    int checkNum = 0;
                    for (int i = 0; i < 10; i++) {
                        JSONObject firstObject = hourly.getJSONObject(i);
                        JSONObject weatherList = firstObject.getJSONArray("weather").getJSONObject(0);
                        //JSONObject tempList = firstObject.getJSONObject("main");
                        String time = firstObject.getString("dt");
                        String descriptionText = weatherList.getString("description");
                        //String max_temp = tempList.getString("temp");
                        //String min_temp = tempList.getString("temp_min");
                        Log.d(TAG, "" + descriptionText);
                        if (descriptionText.equals("晴天")) {

                                //Log.d(TAG, "" + time);
                            days[checkNum] = Long.valueOf(time).longValue();
                            resultText.add(unixTimeChange(time));
                            checkNum++;
                            result += unixTimeChange(time) + " " + descriptionText + "\r\n";

                        }
                        if (descriptionText.equals("薄い雲")) {
                            checkNum++;
                            result += unixTimeChange(time) + " " + descriptionText +  "\r\n";
                        }
                    }
                    /*
                        for (int i = 0; i < 7; i++) {
                            JSONObject firstObject = daily.getJSONObject(i);
                            JSONObject weatherList = firstObject.getJSONArray("weather").getJSONObject(0);
                            String time = firstObject.getString("dt");
                            String descriptionText = weatherList.getString("description");
                            //Log.d(TAG, "" + descriptionText);
                            if (descriptionText.equals("薄い雲")) {
                                if (checkNum < t) {
                                    days[checkNum] = Long.valueOf(time).longValue();
                                    resultText.add(unixTimeChange(time));
                                    checkNum++;
                                }
                            }

                    }*/
                }
            } catch (Exception e) {
                e.printStackTrace();
                //Log.d(TAG, "" + e);
            } finally {
                connection.disconnect();
            }

            handler.post(new Runnable() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void run() {
                    onPostExecute(result, t, days);
                }
            });
        }
    }
    /*
    @Override
    protected String doInBackground(URL... urls) {

        //final URL url = urls[0];
        //HttpURLConnection con = null;

        HttpURLConnection connection = null;

        try {
            URL url = urls[0];
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();



            int responseCode = connection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStreamReader isr = new InputStreamReader(connection.getInputStream());
                BufferedReader br = new BufferedReader(isr);
                String str = br.readLine();
                JSONObject json = new JSONObject(str);
                JSONArray daily = json.getJSONArray("daily");
                for (int i = 0; i < 7; i++) {
                    JSONObject firstObject = daily.getJSONObject(i);
                    JSONObject weatherList = firstObject.getJSONArray("weather").getJSONObject(0);
                    String time = firstObject.getString("dt");
                    String descriptionText = weatherList.getString("description");
                    resultText += unixTimeChange(time) + descriptionText + "\r\n";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "" + e);
        } finally {
            connection.disconnect();
        }
        return resultText;
    }*/

    void execute(URL urls, int t) {
        ExecutorService executorService  = Executors.newSingleThreadExecutor();
        executorService.submit(new AsyncRunnable(urls, t));
    }

    /**
     * 非同期処理が終わった後の処理.
     * @param result 非同期処理の結果得られる文字列
     */
    //@Override
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    protected void onPostExecute(String result, int t, long[] days) {
        //mainActivity.setContentView(R.layout.activity_main);
        //TextView tv = mainActivity.findViewById(R.id.messageTextView);
        //tv.setText(result);
        //Log.d(TAG, "ようこそ " + address);
        //Log.d(TAG, "" + result.size());
        if(result.equals("")) {
            /*TextView tv = mainActivity.findViewById(R.id.text);
            tv.setText("今日の天気は散歩に適していません");*/
            result += "本日の天気はあまりよくありません...";
            //今日の天気は散歩に適していません
            //Log.d(TAG, "直近一週間で快晴の日がありません。");
        }
        //Log.d(TAG, "以下の日が散歩に適しています");
        //int size = result.size();
        switch (t) {
            case 1:
                TextView tc = mainActivity.findViewById(R.id.comment);
                tc.setText("歩行距離が十分です!この調子で頑張りましょう!!");
                TextView tv = mainActivity.findViewById(R.id.text);
                tv.setText(result);
                //Log.d(TAG, result);
                //resultを表示と同時に、1週間の歩行距離が十分であるとコメント、
                //また、アラームを3日後に設定
                //Date date = new Date(days[0]*1000L);
                //Log.d(TAG, "debug: " + date);
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                calendar.set(Calendar.HOUR_OF_DAY, 9);
                calendar.set(Calendar.MINUTE, 0);
                calendar.add(Calendar.DAY_OF_MONTH, 3);
                //calendar.setTime(date);
                //Log.d(TAG, ""+calendar);
                Intent intent6 = new Intent(mainActivity.getApplicationContext(), AlarmNotification.class);
                intent6.putExtra("RequestCode",0);

                pending = PendingIntent.getBroadcast(
                        mainActivity.getApplicationContext(),0, intent6, PendingIntent.FLAG_UPDATE_CURRENT);

                // アラームをセットする
                am = (AlarmManager) mainActivity.getSystemService(Context.ALARM_SERVICE);

                if (am != null) {
                    am.setExact(AlarmManager.RTC_WAKEUP,
                            calendar.getTimeInMillis(), pending);

                    //Log.d(TAG, "アラームを設定しました: "+ result.get(0));

                    // トーストで設定されたことをを表示
                    /*Toast.makeText(mainActivity.getApplicationContext(),
                            "アラームを設定しました: "+ result.get(0), Toast.LENGTH_SHORT).show();*/

                    android.util.Log.d("debug", "start");
                }
                Toast.makeText(mainActivity.getApplicationContext(),
                        "アラームを設定しました" + calendar.getTime() , Toast.LENGTH_LONG).show();
                break;
            case 2:
                TextView tc2 = mainActivity.findViewById(R.id.comment);
                tc2.setText("1週間の歩行距離が少し足りないので、是非散歩に行きましょう!");
                TextView tv2 = mainActivity.findViewById(R.id.text);
                tv2.setText(result);
                //Log.d(TAG, result);
                //resultを表示と同時に、1週間の歩行距離が少し足りないことをコメント、
                //また、アラームを2日後に設定
                /*List<Calendar> alarm_days;
                alarm_days = new ArrayList<>();
                for (long d : days) {
                    Date da = new Date(d * 1000L);
                    //Log.d(TAG, "debug: " + da);
                    Calendar calendar1 = Calendar.getInstance();
                    calendar1.setTime(da);
                    alarm_days.add(calendar1);
                }*/

                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTimeInMillis(System.currentTimeMillis());
                calendar2.set(Calendar.HOUR_OF_DAY, 9);
                calendar2.set(Calendar.MINUTE, 0);
                calendar2.add(Calendar.DAY_OF_MONTH, 2);

                Intent intent = new Intent(mainActivity.getApplicationContext(), AlarmNotification.class);
                intent.putExtra("RequestCode",1);

                pending = PendingIntent.getBroadcast(
                        mainActivity.getApplicationContext(),1, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                // アラームをセットする
                am = (AlarmManager) mainActivity.getSystemService(Context.ALARM_SERVICE);

                if (am != null) {
                    am.setExact(AlarmManager.RTC_WAKEUP,
                            calendar2.getTimeInMillis(), pending);

                    //Log.d(TAG, "アラームを設定しました: "+ result.get(0));

                    // トーストで設定されたことをを表示
                    /*
                    Toast.makeText(mainActivity.getApplicationContext(),
                            "アラームを設定しました: "+ calendar2.getTime(), Toast.LENGTH_SHORT).show();*/

                    android.util.Log.d("debug", "start");
                }
                /*
                Intent intent2 = new Intent(mainActivity.getApplicationContext(), AlarmNotification.class);
                intent.putExtra("RequestCode",2);

                pending2 = PendingIntent.getBroadcast(
                        mainActivity.getApplicationContext(),2, intent2, PendingIntent.FLAG_UPDATE_CURRENT);

                // アラームをセットする
                am2 = (AlarmManager) mainActivity.getSystemService(Context.ALARM_SERVICE);

                if (am2 != null) {
                    am2.setExact(AlarmManager.RTC_WAKEUP,
                            alarm_days.get(0).getTimeInMillis(), pending2);

                    //Log.d(TAG, "アラームを設定しました: "+ result.get(1));

                    // トーストで設定されたことをを表示
                    /*
                    Toast.makeText(mainActivity.getApplicationContext(),
                            "アラームを設定しました: "+ result.get(1), Toast.LENGTH_SHORT).show();*/

                    //android.util.Log.d("debug", "start");


                Toast.makeText(mainActivity.getApplicationContext(),
                        "アラームを設定しました" + calendar2.getTime()
                                , Toast.LENGTH_SHORT).show();
                break;
            case 3:
                TextView tc3 = mainActivity.findViewById(R.id.comment);
                tc3.setText("1週間の歩行距離が十分ではないので、是非散歩に行きましょう!");
                TextView tv3 = mainActivity.findViewById(R.id.text);
                tv3.setText(result);
                //Log.d(TAG, result);
                Calendar calendar3 = Calendar.getInstance();
                calendar3.setTimeInMillis(System.currentTimeMillis());
                //calendar3.set(Calendar.DAY_OF_MONTH, 24);
                calendar3.set(Calendar.HOUR_OF_DAY, 9);
                calendar3.set(Calendar.MINUTE, 0);
                calendar3.add(Calendar.DAY_OF_MONTH, 1);

                TextView db = mainActivity.findViewById(R.id.debug);
                db.setText("DEBUG: " + calendar3.getTime());

                //resultを表示と同時に、1週間の歩行距離が足りないため、可能であれば外出することを勧めるコメント、
                //また、アラームを1日後に設定
                /*
                List<Calendar> alarm_days2;
                alarm_days2 = new ArrayList<>();
                for (long d : days) {
                    Date da = new Date(d * 1000L);
                    //Log.d(TAG, "debug: " + da);
                    Calendar calendar1 = Calendar.getInstance();
                    calendar1.setTime(da);
                    alarm_days2.add(calendar1);
                }*/
                Intent intent3 = new Intent(mainActivity.getApplicationContext(), AlarmNotification.class);
                intent3.putExtra("RequestCode",1);

                pending = PendingIntent.getBroadcast(
                        mainActivity.getApplicationContext(),1, intent3, PendingIntent.FLAG_UPDATE_CURRENT);

                // アラームをセットする
                am = (AlarmManager) mainActivity.getSystemService(Context.ALARM_SERVICE);

                if (am != null) {
                    am.setExact(AlarmManager.RTC_WAKEUP,
                            calendar3.getTimeInMillis(), pending);

                    //Log.d(TAG, "アラームを設定しました: "+ result.get(0));

                    // トーストで設定されたことをを表示
                    /*Toast.makeText(mainActivity.getApplicationContext(),
                            "アラームを設定しました: "+ result.get(0), Toast.LENGTH_SHORT).show();*/

                    android.util.Log.d("debug", "start");
                }
                /*
                Intent intent4 = new Intent(mainActivity.getApplicationContext(), AlarmNotification.class);
                intent4.putExtra("RequestCode",2);

                pending2 = PendingIntent.getBroadcast(
                        mainActivity.getApplicationContext(),2, intent4, PendingIntent.FLAG_UPDATE_CURRENT);

                // アラームをセットする
                am2 = (AlarmManager) mainActivity.getSystemService(Context.ALARM_SERVICE);

                if (am2 != null) {
                    am2.setExact(AlarmManager.RTC_WAKEUP,
                            alarm_days2.get(1).getTimeInMillis(), pending2);

                    //Log.d(TAG, "アラームを設定しました: "+ result.get(1));

                    // トーストで設定されたことをを表示
                    /*
                    Toast.makeText(mainActivity.getApplicationContext(),
                            "アラームを設定しました: "+ result.get(1), Toast.LENGTH_SHORT).show();

                    android.util.Log.d("debug", "start");
                }

                Intent intent5 = new Intent(mainActivity.getApplicationContext(), AlarmNotification.class);
                intent5.putExtra("RequestCode",3);

                pending3 = PendingIntent.getBroadcast(
                        mainActivity.getApplicationContext(),3, intent5, PendingIntent.FLAG_UPDATE_CURRENT);

                // アラームをセットする
                am3 = (AlarmManager) mainActivity.getSystemService(Context.ALARM_SERVICE);

                if (am3 != null) {
                    am3.setExact(AlarmManager.RTC_WAKEUP,
                            alarm_days2.get(2).getTimeInMillis(), pending2);

                    //Log.d(TAG, "アラームを設定しました: "+ result.get(2));

                    // トーストで設定されたことをを表示

                    Toast.makeText(mainActivity.getApplicationContext(),
                            "アラームを設定しました: "+ result.get(1), Toast.LENGTH_SHORT).show();

                    android.util.Log.d("debug", "start");
                }*/
                Toast.makeText(mainActivity.getApplicationContext(),
                        "アラームを設定しました" + calendar3.getTime(), Toast.LENGTH_SHORT).show();
                break;
        }

        //for (String r:result) {
        //    Log.d(TAG, "" + r);
            /*try{
                Event event = new Event()
                        .setSummary("Google I/O 2015")
                        .setLocation("800 Howard St., San Francisco, CA 94103")
                        .setDescription("A chance to hear more about Google's developer products.");

                DateTime startDateTime = new DateTime(r);
                EventDateTime start = new EventDateTime()
                        .setDateTime(startDateTime)
                        .setTimeZone("Asia/Tokyo");
                event.setStart(start);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
                Date d = sdf.parse(r);
                Calendar cal = Calendar.getInstance();
                cal.setTime(d);
                cal.add(Calendar.HOUR, 1);
                String en = unixTimeChange(cal.getTime().toString());
                DateTime endDateTime = new DateTime(en);
                EventDateTime end = new EventDateTime()
                        .setDateTime(endDateTime)
                        .setTimeZone("Asia/Tokyo");
                event.setEnd(end);

                String[] recurrence = new String[] {"RRULE:FREQ=DAILY;COUNT=2"};
                event.setRecurrence(Arrays.asList(recurrence));

                EventAttendee[] attendees = new EventAttendee[] {
                        new EventAttendee().setEmail(address),
                };
                event.setAttendees(Arrays.asList(attendees));

                EventReminder[] reminderOverrides = new EventReminder[] {
                        new EventReminder().setMethod("email").setMinutes(24 * 60),
                        new EventReminder().setMethod("popup").setMinutes(10),
                };
                Event.Reminders reminders = new Event.Reminders()
                        .setUseDefault(false)
                        .setOverrides(Arrays.asList(reminderOverrides));
                event.setReminders(reminders);

                String calendarId = "primary";
                event = service.events().insert(calendarId, event).execute();
            } catch (ParseException e) {
                e.printStackTrace();
            }*/



        //}

    }

    private String unixTimeChange(String unixTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
        Date nowTime = new Date(Integer.parseInt(unixTime) * 1000L);
        //DateTimeFormatter dtf = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
        //Date nowTime = new Date()
        return sdf.format(nowTime);
    }
}