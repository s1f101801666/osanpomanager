package com.example.osanpomanager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Value;
import com.google.android.gms.fitness.request.DataReadRequest;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static com.google.android.gms.fitness.data.DataType.TYPE_STEP_COUNT_DELTA;

public class MainActivity extends AppCompatActivity implements LocationListener {

    private static final int REQUEST_CODE = 12345;
    private static final String API_KEY = "openweathermapのapiキーを入力";
    private double placeLat;
    private double placeLon;
    private LocationManager mLocationManager;
    private String bestProvider;

    FitnessOptions fitnessOptions = FitnessOptions.builder()
            .addDataType(TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
            .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
            .build();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GoogleSignInAccount account = GoogleSignIn.getAccountForExtension(this, fitnessOptions);
        if (!GoogleSignIn.hasPermissions(account, fitnessOptions)) {
            GoogleSignIn.requestPermissions(
                    this, // your activity
                    REQUEST_CODE, // e.g. 1
                    account,
                    fitnessOptions);
        }

        Button buttonStart = this.findViewById(R.id.schedule);
        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    accessGoogleFit(1);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
            }
        });
        Button buttonStart2 = this.findViewById(R.id.mean);
        buttonStart2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    accessGoogleFit(2);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
            }
        });

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,},
                    1000);
        }
        else{
            locationStart();

            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    1000, 50, this);

        }
        //initLocationManager();
    }

    /*@Override
    protected void onStart() {
        super.onStart();

        locationStart();
    }

    @Override
    protected void onStop() {
        super.onStop();

        locationStop();
    }

    private void initLocationManager() {
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setPowerRequirement(Criteria.POWER_HIGH);
        criteria.setSpeedRequired(false);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);
        criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
        bestProvider = mLocationManager.getBestProvider(criteria, true);
    }

    private void checkPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // パーミッションの許可を取得する
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1000);
        }
    }*/

    private void locationStart() {
        /*checkPermission();
        mLocationManager.requestLocationUpdates(bestProvider, 60000, 3, this);*/
        mLocationManager =
                (LocationManager) getSystemService(LOCATION_SERVICE);

        if (mLocationManager != null && mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Log.d("debug", "location manager Enabled");
        } else {
            // GPSを設定するように促す
            Intent settingsIntent =
                    new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(settingsIntent);
            Log.d("debug", "not gpsEnable, startActivity");
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);

            Log.d("debug", "checkSelfPermission false");
            return;
        }

        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                1000, 50, this);
    }

    private void  locationStop() {
        mLocationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        placeLat = location.getLatitude();
        placeLon = location.getLongitude();
        TextView db = findViewById(R.id.debug);
        db.setText("DEBUG: \r\n lat : " + placeLat + "\r\n" + "lon : " + placeLon);
        /*Log.d("DEBUG", "lat : " + placeLat);
        Log.d("DEBUG", "lon : " + placeLon);*/
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[]permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1000) {
            // 使用が許可された
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d("debug", "checkSelfPermission true");

                locationStart();

            } else {
                // それでも拒否された時の対応
                Toast toast = Toast.makeText(this,
                        "これ以上なにもできません", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d("DEBUG", "called onStatusChanged");
        switch (status) {
            case LocationProvider.AVAILABLE:
                Log.d("DEBUG", "AVAILABLE");
                break;
            case LocationProvider.OUT_OF_SERVICE:
                Log.d("DEBUG", "OUT_OF_SERVICE");
                break;
            case LocationProvider.TEMPORARILY_UNAVAILABLE:
                Log.d("DEBUG", "TEMPORARILY_UNAVAILABLE");
                break;
            default:
                Log.d("DEBUG", "DEFAULT");
                break;
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("DEBUG", "called onProviderDisabled");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("DEBUG", "called onProviderEnabled");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE) {
                Toast.makeText(getApplicationContext(),
                        "ようこそ", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void accessGoogleFit(int num) throws ExecutionException, InterruptedException, TimeoutException {
        //Log.d(TAG,"test fit");
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        long endTime = cal.getTimeInMillis();
        cal.add(Calendar.DAY_OF_MONTH, -14);
        long startTime = cal.getTimeInMillis();



        GoogleSignInAccount account = GoogleSignIn
                .getAccountForExtension(this, fitnessOptions);

        DataReadRequest request = new DataReadRequest.Builder()
                .aggregate(TYPE_STEP_COUNT_DELTA)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .bucketByTime(1, TimeUnit.DAYS)
                // .read(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                .build();

        Fitness.getHistoryClient(this, account)
                .readData(request)
                .addOnSuccessListener(response -> {
                    // Use response data here
                    List<Bucket> mb;
                    mb = new ArrayList<Bucket>();
                    mb.addAll(response.getBuckets());
                    //Log.d(TAG, "test fit second");
                    int x = 0;
                    int x2 = 0;
                    int n = mb.size();
                    int cnt = 0;
                    //Log.d(TAG, ""+n);
                    //DataSet dataSet = response.getDataSet(DataType.TYPE_STEP_COUNT_CUMULATIVE);
                    for (Bucket bucket : mb) {
                        //Log.d(TAG, "test fit third");
                        long st = bucket.getStartTime(TimeUnit.MILLISECONDS);
                        long ed = bucket.getEndTime(TimeUnit.MILLISECONDS);
                        Date d1 = new Date(st);
                        Date d2 = new Date(ed);
                        /*String st2 = String.valueOf(st);
                        String ed2 = String.valueOf(ed);*/
                        try {
                            DataSet dataset = bucket.getDataSet(DataType.AGGREGATE_STEP_COUNT_DELTA);
                            Value value = dataset.getDataPoints().get(0).getValue(Field.FIELD_STEPS);
                            //x++;
                            if (cnt >= 7) {
                                x = x + Integer.parseInt(value.toString());
                                cnt++;
                            } if (cnt <=8) {
                                x2 = x2 + Integer.parseInt(value.toString());
                                cnt++;
                            }

                            //Log.i(TAG, ""+ d1 + "\r\n" + d2 + "\r\n" + value.toString());
                        }
                        catch (IndexOutOfBoundsException e) {
                            e.printStackTrace();
                            cnt++;
                            //Log.d(TAG, "" + e);
                            //Log.d(TAG, "" + d1 + "\r\n" + d2 + "\r\n" + 0);
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                            TextView db = findViewById(R.id.debug);
                            db.setText("Error: " + e);
                            //Log.d(TAG, "" + e);
                        }
                        //Log.i(TAG, "" + x);

                    }
                    int m = x / 7;
                    int m2 = x2 / 7;
                    //Log.i(TAG, "7日間の平均: " + m);
                    int t = 1;
                    if (m <= 5000) {
                        t = 3;
                    } if (m > 5000 & m <= 8000) {
                        t = 2;
                    }
                    /*
                    List<Date> d;
                    d = new ArrayList<>();
                    for (int i = 0; i < t; i++) {
                        Random r = new Random();
                        int day = r.nextInt(4) + 1;
                        cal.setTime(new Date());
                        cal.add(Calendar.DAY_OF_MONTH, +day);
                        long tt = cal.getTimeInMillis();
                        d.add(new Date(tt));
                    }
                    for (Date j : d) {
                        //Log.i(TAG, "" + j);
                    }*/
                    //Log.i(TAG, "ここを天気APIと絡めて表示する");
                    /*
                    String requestURL = "https://api.openweathermap.org/data/2.5/onecall?" +
                            "lat=" + placeLat + "&" +
                            "lon=" + placeLon + "&" +
                            "lang=" + "ja" + "&" +
                            "APPID=" + API_KEY;
                    HttpURLConnection connection = null;
                    try {
                        URL url = new URL(requestURL);
                        connection = (HttpURLConnection) url.openConnection();
                        connection.setRequestMethod("GET");
                        connection.connect();

                        int responseCode = connection.getResponseCode();

                        if (responseCode == HttpURLConnection.HTTP_OK) {
                            InputStreamReader isr = new InputStreamReader(connection.getInputStream());
                            BufferedReader br = new BufferedReader(isr);
                            String str = br.readLine();
                            JSONObject json = new JSONObject(str);
                            JSONArray hourly = json.getJSONArray("hourly");
                            for (int i = 0; i < 9; i++) {
                                JSONObject firstObject = hourly.getJSONObject(i);
                                JSONObject weatherList = firstObject.getJSONArray("weather").getJSONObject(0);
                                String time = firstObject.getString("dt");
                                String descriptionText = weatherList.getString("description");
                                resultText += unixTimeChange(time) + descriptionText;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d(TAG, "" + e);
                    } finally {
                        connection.disconnect();
                    }*/
                    //Log.d(TAG, resultText);
                    if (num == 1) {
                        try {
                            new AsyncHttpRequest(this).execute(new URL("https://api.openweathermap.org/data/2.5/onecall?" +
                                    "lat=" + placeLat + "&" +
                                    "lon=" + placeLon + "&" +
                                    "lang=" + "ja" + "&" +
                                    "APPID=" + API_KEY), t);
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    } if (num == 2) {
                                    TextView tm = findViewById(R.id.text_mean);
                                    tm.setText("1週間の平均: " + m + " 1週間の合計: " + x + "\r\n"
                                            + "前の週の平均: " + m2 + " 前の週の合計: " + x2);

                    }



                    /*dataSet.getDataPoints().forEach(point ->
                    );*/
                    //Log.d(TAG, "OnSuccess()");
                })
                .addOnFailureListener(e -> {
                   // Log.d(TAG, "OnFailure()", e);
                });
    }
}